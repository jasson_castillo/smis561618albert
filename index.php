<?php
include_once "clases/Alumno.class.php";
$alumno= new Alumno(); //instancia de la clase Alumno

/*
echo "<pre>";
var_dump($alumno->getAll());
echo "</pre>";
*/

$datos= $alumno->getAll();
echo "<h1>Listado de alumnos</h1>";
echo "<a href='registroAlumno.php'>Nuevo registro....</a>";
echo "<table border='1'>
        <tr>
            <th>Nombre</th>
            <th>Documento</th>
            <th>Sexo</th>
            <th>Telefono</th>
            <th>Correo</th>
            <th>Direccion</th>
            <th>Eliminar</th>
        </tr>";

//lectura del arreglo
foreach($datos as $key=>$item){
    echo "<tr>";    
    $codigo= $item['codAlumno'];
    echo "<td>". $item['nombreCompleto'] ."</td>";
    echo "<td>". $item['numeroDocumento'] ."</td>";
    echo "<td>". $item['sexo'] ."</td>";
    echo "<td>". $item['numeroTelefono'] ."</td>";
    echo "<td>". $item['correo'] ."</td>";
    echo "<td>". $item['direccion'] ."</td>";
    echo "<td><a href='editarAlumno.php?cod=".$codigo."'>Editar</a></td>";
    echo "<td><a href='javascript:respuesta()'>Eliminar</a></td>";
}
echo "</tr></table>";
?>
<script type="text/javascript">
    function respuesta(){
        var codigo = '<?php echo $codigo; ?>';
        var resp = confirm("Esta seguro que desea eliminar el registro?");
        if(resp==true){
            window.location.href="eliminarAlumno.php?cod="+codigo+"";
        }
    }
</script>